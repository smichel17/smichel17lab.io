# Contributor Guide

- [How to contribute](#how-to-contribute)
- [Project structure](#project-structure)
- [Style guide](#style-guide)
- [Using PieCrust](#using-piecrust)
- [Deployment](#deployment)
- [Tools](#tools)
- [Page format conversion](#page-format-conversion)


## How to contribute

- Please speak with the [design team] about the task or suggestion.
- Fork the repo, make the changes and open a merge request. Thanks!

[design team]: https://gitlab.com/snowdrift/design/-/wikis/home


## Project structure

- `assets/`: contains all stylesheets, fonts and images
  (in production `website/static/`)

- `pages/`: html for individual pages
  (in production `website/templates/page/`)

- `public/`: generated static pages

- `src/`: asset sources added after the original mockups

- `templates/`: html/jinja for page layout templates
  (in production `website/templates/main/`)

- `.gitlab-ci.yml`: configuration for GitLab CI deployment
   (to https://sdproto.gitlab.io)

- `config.yml`: site settings and template variables

### Stylesheets

The stylesheets are divided into 2 subdirectories, `main/` and `page/`.
`main/` refers to the main template, and contains global patterns and
declarations. `page/` contains per-page styles, with a similar folder structure
as `pages/` in the upper level.

Highlights from the main stylesheets:

- `devel.sass`: development-only features (see the Tools section)

- `main.sass`: imports all the Sass partials for the main template

- `_footer.sass`, `_navbar[--<width>].sass`: styles for the footer and
  navbar template partials

- `_page.sass`: calls the font-face declarations and resets elements for pages
  that use the template

- `_variables.sass`: variables and asset paths used throughout pages

- `_mixins.sass`: styles that are re-used in multiple classes across the site

- `_page.sass`: common classes that are used on many pages across the site,
  such as font presets and UI components; especially default styles.

### Templates

Page templates are made with the [Jinja] templating engine. Variables set in
`config.yml` can be referenced within the templates and in the page contents.

### Assets

Assets are sorted by type, such as css and images, followed by page. Assets for
development mode only are stored in `assets/devel/`. Export png images at 384
dpi (or multiple of 96 dpi) to provide reasonably crisp outlines at 200% level
and Retina screens. Compress them with utilities like pngquant to reduce file
sizes.


## Using PieCrust

- The built-in server can watch for stylesheet and page changes. It listens on
  port 5001 by default. The hostname and port can be changed by running the
  PieCrust executable itself: `venv/bin/chef serve -a localhost -p 8080`

- In the event a page template or stylesheet stops updating while running the
  built-in server, exit the server. Run `make clean` to clear the cache, and
  `make` to rebuild the pages and re-run the server.


## Deployment

A GitLab CI config is included for hosting a static version on GitLab Pages.
The file is pre-configured to deploy from the master branch.

**Note**: make sure GitLab Pages is configured with a domain/sub-domain top
level, e.g. `project.gitlab.io`, not `example.gitlab.io/project/`. Relative
links are unsupported, so links will be broken if the site sits in a
subdirectory.

Build the static site: `make`

Commit to the master branch (or another branch if the CI has been configured to
watch other branches) and push remotely.


## Style guide

This section borrows heavily from ideas explained in [CSS Guidelines]
(v2.2.5 as of current writing). The goal is to promote style reuse and write
consistent, maintainable stylesheets.

- Suggested column width: 79 characters. Exceptions: long declarations like
  font sources, urls, gradients, etc.
  - Why: allow multiple files to be displayed on a wide screen, and easier
    reading on smaller screens without side-scrolling.

- Indentation: 4 spaces for Sass, 2 spaces for HTML.
  - Why: distinguish between parent and child selector rules at a cursory
    glance in Sass. HTML tends to be heavily nested, so a smaller indentation
    provides more horizontal space.

- Nesting: keep Sass to a maximum of 2 levels of indentation where possible, not counting media queries.

  ```
  .main-section
      // Good
      .__header
          font-size: $size_medium

      // Okay
      +respond--mini
          .__text
              padding: 0 3rem

      // Bad
      .__table
          display: grid

          .__option
              grid-column: 2
  ```

  - Why: help reduce over-qualifying and is more readable.

- Nest media queries under the associated component block where applicable.
  - Why: keep components self-contained for easier searching and refactoring.

- Do not nest media queries under elements in a block (do nest elements under the query).  
  Exception: ok to nest if the block has only one element that uses media queries.
  - Why: easier to understand how media queries affect component layout.

- Use `//` for comments in sections that are not visible when compiled,
  such as Sass variable declarations. Use `/* */` for comments that may be
  helpful for debugging.
  - Why: remove clutter from compiled stylesheets to make them more searchable.

- Whitespace: 1 line between groups of declarations. 2 lines between groups in
  different content sections if preferred. 2 lines between different functional
  sections, e.g. between page selectors and media queries.
  - Why: increase readability.

- Declaration order: group by positional and cosmetic declarations. Put any
  mixin calls and variables at the top. Example:

  ```sass
  .status-box
      // Mixins, variables
      +text("body")
      $bronze: #8d9400

      // Position
      position: relative
      z-index: 2
      top: 9rem
      right: 3rem
      bottom: 6rem
      left: 3rem

      // Display, grid, box
      display: block
      overflow: hidden
      width: 72rem
      height: 33rem
      margin: 0 auto
      padding: 3rem
      border-bottom: 0.4rem solid $bronze
      border-radius: 0.3rem

      // Cosmetic
      background: #fdffc7
      color: $bronze
      font-weight: 700
      text-decoration: none
  ```

  - Why: help people unfamiliar with the stylesheets to locate a declaration
    and write consistent groups of declarations.  
    Also: Keeps properties likely to affect children near the bottom (thus, likely nearer the css for those children), so related css stays as close together as possible.

- The selector naming convention is derived from [BEM] and [rcss]. The basic
  components are:

  - *Blocks* — groups of elements forming an interface mechanism or serving
    a common purpose. Names are two words hyphenated, e.g. `.footer-nav`. Start
    new blocks at top-level, even when they form a part of a larger block.

  - *Elements* — parts of a block. Names are prefixed by two underscores,
    e.g.  `.__logo`.

  - *Modifiers* — block or element variants. Names are prefixed by two hyphens,
    e.g.  `.--small`.

  - Why: show relationships among groups of elements, help to understand what
    assigned properties selectors provide.

- Global variables and mixins should only be declared in `_variables.sass` and `_mixins.sass`, respectively.
    - Why: help locate the source of a declaration

- Use classes (`.foo`).
  - Why: they're efficient and portable.

- Use type selectors sparingly, especially top-level (e.g. `a`).
  - Why: they depend on the element to not change type; changes in one section
    can cause undesired behaviour on other sections of a page.

- Use `!important` very sparingly. Any use of it should be initiative, not
  reactive, and must be documented in the code.
  - Why: demonstrate clear need for blocking all capacity to override the
    declaration, especially when the element contains child elements. Reactive
    uses can later cause extensibility issues with unexpected additions, and
    reduces readability when tracing paths of inheritance.

- Avoid IDs (`#foo`). Exception: navigation anchors.
  - Why: they're more difficult to override if there are changes later, and the
    declarations aren't reusable.

[CSS Guidelines]: https://cssguidelin.es/
[BEM]: https://en.bem.info/methodology/
[rcss]: https://rscss.io/
[Jinja]: https://jinja.palletsprojects.com/


## Tools

Custom tools are listed here.

### Grid overlay

- Usage: move the mouse to the top-left corner of a page. A button will
  appear that can be clicked to toggle the grid. The grid changes according to
  the major breakpoints.

### Mockup overlay

- Setup: export the page mockup to .png at 96 dpi and copy it to
  `assets/devel/mockups/`. The filename should follow the convention
  `alpha_[page slug].png`, e.g. `alpha_auth_create-account.png` for a page at
  `auth/create-account`. Rebuild the site.

- Usage: move the mouse to the top-left side of a page (below the grid overlay
  button). A button will appear that can be clicked to toggle the mockup. In
  some cases it may be necessary to manually align the mockup position in the
  web browser's inspector.


## Page format conversion

Content for certain pages may be in another format. Below are recipes to handle
conversion among formats during implementation.

**Note:** when importing content updates from canonical sources in another
format, remember to re-insert semantic classes to reconnect any existing style
declarations such as headers, content containers, table of contents and
footnotes. Restore any image links.

### Markdown to HTML

- Install the `markdown` package for your distribution. If you require support
  for footnotes, look for `pandoc`.

- Convert: `markdown -html5 [input].md > [output].html`

### Hamlet to HTML

- Convert: `./utils/lazyham.sh [input].hamlet`

### HTML to Hamlet

Use [html2hamlet](https://github.com/tanakh/html2hamlet) or another
[fork](https://github.com/benjamingoldstein/html2hamlet).

- Fetch the source: `git clone https://github.com/tanakh/html2hamlet && cd
  html2hamlet`

- Edit `Html2Hamlet.hs` to remove the html5 doctype declaration, which is
  unnecessary for page files in production, and use the number of spaces for
  indentation as recommended in production:

  ```
  cvt doc = go doc
  go cur = fromNode (node cur) <$$> indent 2 (vsep (map go $ child cur))
  ```

- Install dependencies and build. Here it is reusing the same resolver lts
  version as production for convenience, e.g. `--resolver=lts-9.18`:

  ```
  stack install cabal-install --resolver=lts-x.xx
  stack init --resolver=lts-x.xx --solver
  stack build
  ```

- Convert: `stack exec html2hamlet [input].hamlet`

### Important canonical sources

- [/privacy](https://gitlab.com/snowdrift/snowdrift/-/blob/master/website/templates/page/privacy.hamlet)
- [/terms](https://gitlab.com/snowdrift/legal/-/blob/master/terms-of-service.md)
- [/trademarks](https://gitlab.com/snowdrift/snowdrift/-/blob/master/website/templates/page/trademarks.hamlet)
