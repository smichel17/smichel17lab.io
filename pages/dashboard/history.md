---
title: Dashboard
---

<main class="main-content">
  <header class="main-header --tilt">
    <h1 class="__title">Dashboard</h1>
    {% include "dashboard-nav.html" %}
  </header>

  <a class="now-link" href="/dashboard/now">Now</a>
  <div class="month-box">
    <div class="__headline" id="2018-01"><b>JANUARY </b><span>2018 </span></div>
    <div class="__limit">Limit was $10</div>

    <div class="__project-name"><a href="/project">Snowdrift.coop</a></div>
    <div class="__project-pledge">$&thinsp;2.75</div>
    <div class="__project-patrons">&thinsp;2748</div>

    <div class="__project-name"><a href="/project">Another Project</a></div>
    <div class="__project-pledge">$&thinsp;6.82</div>
    <div class="__project-patrons">&thinsp;6824</div>

    <div class="__projects-sum">$&thinsp;9.57</div>
  </div>
  <div class="fee-charge">
    <div class="__fee">Stripe fee:<span>$&thinsp;0.58</span></div>
    <div class="__charge">Charge:<span>$&thinsp;10.15</span></div>
    <a class="__overcharge" href="#">(Why above $10?)</a>
  </div>
  <div class="month-box">
    <div class="__headline"><b>DECEMBER </b><span>2017 </span></div>
    <div class="__limit">Limit was $10</div>
    <div class="__project-name"><a href="/project">Snowdrift.coop</a></div>
    <div class="__project-pledge">$&thinsp;1.35</div>
    <div class="__project-patrons">&thinsp;1534</div>

    <div class="__project-name"><a href="/project">Another Project</a></div>
    <div class="__project-pledge">$&thinsp;6.78</div>
    <div class="__project-patrons">&thinsp;6784</div>

    <div class="__project-name dropped"><a href="/project">Project XYZ</a></div>
    <div class="__project-pledge dropped">Dropped!</div>
    <div class="__project-patrons dropped">&thinsp;5768</div>

    <div class="__projects-sum">$&thinsp;8.31</div>
  </div>
  <div class="fee-charge">
    <div class="__fee">Stripe fee:<span>$&thinsp;0.54</span></div>
    <div class="__charge">Charge:<span>$&thinsp;8.85</span></div>
  </div>
  <div class="month-box">
    <div class="__headline"><b>NOVEMBER </b><span>2017 </span></div>
    <div class="__limit">Limit was $10</div>
    <div class="__project-name"><a href="/project">Snowdrift.coop</a></div>
    <div class="__project-pledge">$&thinsp;1.08</div>
    <div class="__project-patrons">&thinsp;1075</div>

    <div class="__project-name"><a href="/project">Another Project</a></div>
    <div class="__project-pledge">$&thinsp;4.36</div>
    <div class="__project-patrons">&thinsp;4357</div>

    <div class="__project-name"><a href="/project">Project XYZ</a></div>
    <div class="__project-pledge">$&thinsp;0.20</div>
    <div class="__project-patrons">&thinsp;210</div>

    <div class="__projects-sum">$&thinsp;5.64</div>
  </div>
  <div class="fee-charge">
    <div class="__fee">Stripe fee:<span>$&thinsp;0.46</span></div>
    <div class="__charge">Charge:<span>$&thinsp;6.10</span></div>
  </div>
  <div class="month-box">
    <div class="__headline"><b>OCTOBER </b><span>2017 </span></div>
    <div class="__limit">Limit was $5</div>
    <div class="__project-name"><a href="/project">Snowdrift.coop</a></div>
    <div class="__project-pledge">$&thinsp;0.98</div>
    <div class="__project-patrons">&thinsp;975</div>

    <div class="__project-name"><a href="/project">Another Project</a></div>
    <div class="__project-pledge">$&thinsp;3.42</div>
    <div class="__project-patrons">&thinsp;3423</div>

    <div class="__projects-sum">$&thinsp;4.40</div>
  </div>
  <div class="fee-charge">
    <div class="__carryover"><a href="#"><b>SEPTEMBER</b> 2017</a>:<span>$&thinsp;1.22</span></div>
    <div class="__carryover"><a href="#"><b>AUGUST</b> 2017</a>:<span>$&thinsp;0.82</span></div>
    <div class="__carryover"><a href="#"><b>JULY</b> 2017</a>:<span>$&thinsp;2.25</span></div>
    <div class="__fee">Stripe fee:<span>$&thinsp;0.43</span></div>
    <div class="__charge">Charge:<span>$&thinsp;9.12</span></div>
    <a class="__overcharge" href="#">(Why above $5?)</a>
  </div>

  <div class="month-box">
    <div class="__headline"><b>SEPTEMBER </b><span>2017 </span></div>
    <div class="__limit">Limit was $5</div>
    <div class="__project-name"><a href="/project">Snowdrift.coop</a></div>
    <div class="__project-pledge">$&thinsp;0.43</div>
    <div class="__project-patrons">&thinsp;425</div>

    <div class="__project-name"><a href="/project">Another Project</a></div>
    <div class="__project-pledge">$&thinsp;0.79</div>
    <div class="__project-patrons">&thinsp;789</div>

    <div class="__projects-delay">charged in <a href="#"> <b>OCTOBER</b> 2017</a></div>
    <div class="__projects-sum">$&thinsp;1.22</div>
  </div>

  <div class="month-box">
    <div class="__headline"><b>AUGUST </b><span>2017 </span></div>
    <div class="__limit">Limit was $5</div>
    <div class="__project-name"><a href="/project">Snowdrift.coop</a></div>
    <div class="__project-pledge">$&thinsp;0.30</div>
    <div class="__project-patrons">&thinsp;301</div>

    <div class="__project-name"><a href="/project">Another Project</a></div>
    <div class="__project-pledge">$&thinsp;0.52</div>
    <div class="__project-patrons">&thinsp;523</div>

    <div class="__projects-delay">charged in <a href="#"> <b>OCTOBER</b> 2017</a></div>
    <div class="__projects-sum">$&thinsp;0.82</div>
  </div>

  <div class="month-box">
    <div class="__headline"><b>JULY </b><span>2017 </span></div>
    <div class="__limit">Limit was $5</div>
    <div class="__project-name"><a href="/project">Snowdrift.coop</a></div>
    <div class="__project-pledge">$&thinsp;0.21</div>
    <div class="__project-patrons">&thinsp;214</div>

    <div class="__projects-delay">charged in <a href="#"> <b>OCTOBER</b> 2017</a></div>
    <div class="__projects-sum">$&thinsp;0.21</div>
  </div>

</main>
