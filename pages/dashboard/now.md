---
title: Dashboard
---

<main class="main-content">
  <header class="main-header --tilt">
    <h1 class="__title">Dashboard</h1>
    {% include "dashboard-nav.html" %}
  </header>

  <p class="info-text">
    <b>FEBRUARY </b><span>2018 </span> crowdmatch will be on February 28.
  </p>
  <div class="month-box">
    <div class="__headline">Your pledges now</div>
    <div class="__limit">
      <div class="__limit-title">Limit</div>
      <form class="__limit-form">
        <input class="__limit-minus" type="submit" value="-"><input class="__limit-value" type="text" value="$10"><input class="__limit-minus" type="submit" value="+">
      </form>
      <div class="__limit-bar">
        <div class="__limit-percent" style="height: 72%"></div>
      </div>
    </div>
    <div class="__project-name"><a href="/p/snowdrift">Snowdrift.coop</a></div>
    <div class="__project-pledge">$&thinsp;1.21</div>
    <div class="__project-patrons">&thinsp;1209</div>
    <div class="__project-name">
      <a href="/p/snowdrift">Another project of longer name</a>
    </div>
    <div class="__project-pledge">$&thinsp;6.78</div>
    <div class="__project-patrons">&thinsp;6784</div>
    <div class="__project-name"><a href="/p/snowdrift">Project XYZ</a></div>
    <div class="__project-pledge">$&thinsp;0.24</div>
    <div class="__project-patrons">&thinsp;248</div>
    <div class="__projects-sum">$&thinsp;7.22</div>
  </div>
  <div class="fee-charge">
    <div class="__fee">Stripe fee:<span>$&thinsp;0.51</span></div>
    <div class="__charge">Charge would be:<span>$&thinsp;7.73</span></div>
  </div>
  <p class="__payment-title">Payment processing fee</p>
  <p class="__payment-body">Each month, we set your donations based on pledge values at that time. In order to keep processing fees under 10% for any charge, we may delay processing of small donations and then combine multiple months' donations in a single charge (adjusting to make sure all charges stay within your budget limit).</p>
  <p class="__limit-info-title">Crowdmatch limit (*currently fixed to $10)</p>
  <p class="__limit-info-body">You will never spend more than this monthly limit. Projects that don't fit into this limit – get dropped.</p>
</main>
