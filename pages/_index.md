---
title: Crowdmatching for Public Goods
---

<main class="main-content">
  <header class="main-header --logo">
    <h1 class="__logo">Snowdrift.coop</h1>
    <div class="__slogan">Crowdmatching for public goods</div>
  </header>

  <!-- [FIXME] Upload subtitles .vtt to archive.org and update the src
       https://archive.org/details/snowdrift-dot-coop-intro -->
  <video class="intro-video" id="intro-video" controls
  poster="/img/index/video-poster-nojs.png" onclick="toggleVideo();">
    <source type="video/mp4"
    src="https://archive.org/download/snowdrift-dot-coop-intro/snowdrift-dot-coop-intro.mp4">
    <source type="video/ogg"
    src="https://archive.org/download/snowdrift-dot-coop-intro/snowdrift-dot-coop-intro.ogv">
    <track kind="subtitles" srclang="en" label="English"
    src="/videos/index/snowdrift-intro-subtitles.vtt">
  </video>

  <section class="sign-up">
    <a class="__button" href="/auth/login">Sign Up</a>
    <a class="__link" href="/how-it-works">learn more</a>
  </section>

  <section class="intro-text">
    <h2 class="__heading">Funding free/libre/open projects</h2>
    <p class="__desc">
      We support works that everyone can use, adapt, and share freely. But as
      public goods, anyone can freeride. And that presents a dilemma in getting
      funded. Our innovative platform brings people together to address this
      problem.
    </p>
    <h2 class="__heading">Matching and sustainability</h2>
    <p class="__desc">
      In a single pledge, crowdmatching combines the two most effective
      solutions for getting voluntary donations to public goods:
    </p>
    <p class="__desc">
      (A) matching donations where patrons agree to give together and
    </p>
    <p class="__desc">
      (B) sustaining memberships which provide reliable, long-term salaries and
      accountability.
    </p>
    <h2 class="__heading">Democracy</h2>
    <p class="__desc">
      We don't serve the concentrated power of advertisers, investors, or
      wealthy philanthropists. Both in our technical design and in running as a
      non-profit co-op, we work to empower regular citizens. By coordinating
      the resources of large numbers of patrons, we can ensure that the
      creative work we fund serves the public interest.
    </p>
  </section>

  <section class="sign-up">
    <div class="__img"></div>
    <a class="__button" href="/auth/login">Sign Up</a>
    <a class="__link" href="/how-it-works">learn more</a>
  </section>

  <script>
    // @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-v3-or-Later
    // ^ librejs compatibility
    var vid;
    window.addEventListener('DOMContentLoaded', function() {
      vid = document.getElementById("intro-video");

      // Hide browser controls and use poster with styled play "button" instead
      vid.removeAttribute('controls');
      vid.setAttribute("poster", "/img/index/video-poster.png");
    }, true);

    // Toggle video playback
    var toggleVideo = function() {
      if (vid.paused) { vid.play(); return false; }
      else { vid.pause(); return false; }
    };
    // @license-end
  </script>
</main>
