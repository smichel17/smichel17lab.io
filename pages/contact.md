---
title: Contact
---

<main class="main-content --tilt">
  <header class="main-header --tilt">
    <h1 class="__title">Contact Snowdrift.coop</h1>
  </header>

  <section class="contact-options">
    <p class="__text">As a distributed team with a global pool of volunteers,
    we communicate almost entirely online. Communication options:</p>

    <h2 class="__heading">Community Forum</h2>
    <p class="__text"> We run an instance of the wonderful Discourse software
    at <a class="__link"
    href="https://community.snowdrift.coop/">community.snowdrift.coop</a> where
    you can connect with the team and community on any related topic.</p> <p
    class="__text">The forum log-in and registration uses the same account as
    the main Snowdrift.coop site.</p>

    <h2 class="__heading">Email</h2>
    <p class="__text"> Although the forum is generally best and has
    private-message features too, you may email us at community@snowdrift.coop
    for support, general questions, privacy concerns etc.</p>

    <h2 class="__heading">Live chat</h2>
    <p class="__text">We have a public IRC channel <code
    class="__channel">#snowdrift</code> at Freenode.net which is also bridged
    to Matrix.org <code class="__channel">#snowdrift:matrix.org</code>.</p>
    <p class="__text">If you have JavaScript enabled, a portal to our channel
    should open right below. Just pick a username and say "hi".</p>

    <p class="__text">See our <a class="__link"
    href="https://wiki.snowdrift.coop/community/irc">IRC wiki page</a> has more
    details and options.</p>

    <iframe class="irc-client"
    src="https://kiwiirc.com/client/irc.freenode.net/#snowdrift"></iframe>
  </section>
</main>
