.PHONY: help install build watch server clean uninstall
.DEFAULT_GOAL:= watch

VENV=venv
PYTHON_VER:=$(shell ls /etc | grep python | cut -d" " -f2 | cut -d"." -f1 | tail -n -1)
PYTHON_WHICH=$(shell which python)
PORT=5001
DEPLOY=public

# Check python version
ifeq ($(PYTHON_VER),python2)
	@echo "Piecrust requires python3. Please install it and retry again." \
		&& exit 1
endif
ifeq ($(PYTHON_WHICH),python)
	PYTHON=python
else
	PYTHON=python3
endif

help:
	@echo "Usage:"
	@echo ""
	@echo "make help                Show this help message"
	@echo "make install             Set up venv and install piecrust"
	@echo	"make build								Clear build cache and rebuild"
	@echo "make watch               Build prototype and run server on localhost"
	@echo "make server              Build prototype and run server on 0.0.0.0"
	@echo "make clean               Clear build cache"
	@echo "make uninstall           Remove venv"

install:
	${PYTHON} -mvenv ${VENV}
	${VENV}/bin/pip install piecrust

build:
	rm -rf _cache ${DEPLOY}
	${VENV}/bin/chef bake -o ${DEPLOY}

watch:
	${VENV}/bin/chef bake -o ${DEPLOY}
	${VENV}/bin/chef serve -p ${PORT}

server:
	${VENV}/bin/chef bake -o ${DEPLOY}
	${VENV}/bin/chef serve -a 0.0.0.0 -p ${PORT}

clean:
	rm -rf _cache ${DEPLOY}

uninstall:
	${VENV}/bin/deactivate
	rm -rf ${VENV}
