#!/bin/bash
# Description: a script to export a folder of .svg files to .png and compress
# them. Requires inkscape and pngquant.
#
# Usage (single file)
# - Make script executable: chmod +x ./export-svg.sh
# - Run script: ./export-svg.sh [file]
#
# Usage (batch export):
# - Create a folder called "svg" in the same directory level as the script and
#   place the svg files in it
# - Make script executable: chmod +x ./export-svg.sh
# - Run script: ./export-svg.sh


_self="$(readlink -f $0)"
proj_dir=$(dirname $_self)
input_dir="$proj_dir/svg"
output_dir="$proj_dir/png"

case "$1" in
  *.svg) # Export a single file
    inkscape -zC --export-png="${1%.*}.png" "$1"
    pngquant "${1%.*}.png" -fo "${1%.*}.png"
    ;;

  "") # Batch export directory of files
    if [ ! -d $input_dir ]; then
      echo -e "Error: source directory doesn't exist.\n\
Please create a folder called \"svg\" in the same directory\n\
as the script and place your svg files inside."; exit 1
    fi
    test -d $output_dir || mkdir $output_dir
    for img in $(ls "$input_dir"); do
        ext="${img##*.}"
        png="${img%.*}.png"
        shopt -s nocasematch
        case $ext in
        svg)
            echo "$img"
            inkscape -zC --export-png="$output_dir/$png" "$img"
            pngquant "$output_dir/$png" -fo "$output/$png"
            ;;
          *) echo "Error: unrecognised image extension."; exit 1;;
        esac
    done
    ;;

  *) # Show usage
    echo "Usage: ./export-svg.sh [file]";;
esac
